<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;
use App\Models\PauModel;
use App\Models\CiclosModel;

/**
 * Description of Cesta
 *
 * @author a054292822b
 */
class Cesta extends BaseController{
    public function index(){
        $pauModel = new PauModel();
        $data['titol'] = "Listado Solicitudes";
        $data['solicitudes'] = $pauModel
                ->select("nif, CONCAT(pau.apellido1,' ',pau.apellido2,', ',pau.nombre) as solicitante, email, pau.id, email, ciclos.nombre, ciclo, ciclos.id as id_ciclo, familia, tipo_tasa")
                ->join('ciclos','ciclos.id=pau.ciclo','LEFT')
                ->findAll();
        return view('solicitudes/lista',$data);
}
 
    public function cesta(){
        $data['cestas'] = $cestaModel;
        echo view('solicitudes/cesta', $data);
    }

    public function borraCesta(){
        $this->session->remove('cesta');
        return redirect()->to(site_url('pauController'));
    }
    
    public function actualizar() {

 if ($this->input->post('actualizar')) {
      $this->cesta->update();
   }
   return redirect('solicitudes/cesta', 'actualizar');
}
}